package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

/**
 * Created by Elizabeth on 5/16/2014.
 */
public class SelectDirectoryActivity extends Activity {
    protected EditText downloadDirEditText;
    protected ImageButton folderSelectImageButton;
    protected Button saveButton;
    protected Button cancelButton;
    protected String directory;
    private final int REQUEST_CODE_PICK_DIR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_directory);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        downloadDirEditText = (EditText)findViewById(R.id.download_dir);
        folderSelectImageButton = (ImageButton)findViewById(R.id.folder_manager);

        String defaultValue = getApplicationContext().getResources()
                .getString(R.string.default_download_dir);
        SharedPreferences sharedPreferences = (SharedPreferences) getApplicationContext()
                .getSharedPreferences("myPrefs", MODE_PRIVATE);
        directory = sharedPreferences.getString(SettingsActivity.KEY_DOWNLOAD_DIR, defaultValue);
        if(directory.equals("")) {
            directory = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        downloadDirEditText.setText(directory);

        folderSelectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fileExploreIntent = new Intent(
                        FileBrowserActivity.INTENT_ACTION_SELECT_DIR,
                        null,
                        getApplicationContext(),
                        FileBrowserActivity.class);
                fileExploreIntent.putExtra(FileBrowserActivity.startDirectoryParameter, directory);
                startActivityForResult(fileExploreIntent, REQUEST_CODE_PICK_DIR);
            }
        });

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getApplicationContext()
                        .getSharedPreferences("myPrefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(SettingsActivity.KEY_DOWNLOAD_DIR,
                        downloadDirEditText.getText().toString());
                editor.apply();
                SelectDirectoryActivity.this.finish();
            }
        });
        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectDirectoryActivity.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_DIR) {
            if(resultCode == this.RESULT_OK) {
                String newDir = data.getStringExtra(
                        FileBrowserActivity.returnDirectoryParameter);
                Toast.makeText(
                        this,
                        "Received DIRECTORY path from file browser:\n" + newDir,
                        Toast.LENGTH_LONG).show();
                downloadDirEditText.setText(newDir);

            } else {
                Toast.makeText(
                        this,
                        "Received NO result from file browser",
                        Toast.LENGTH_LONG).show();
                downloadDirEditText.setText("");
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
}
