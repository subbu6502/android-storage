package com.meygam.cloudstorage.s3.apps;

/**
 * Created by Elizabeth on 5/9/2014.
 */
public class AccountDB {
    private AccountList accountList = new AccountList();

    private static AccountDB instance = new AccountDB();

    public AccountList getAccountList() {
        return accountList;
    }

    public void setAccountList(AccountList accountList) {
        this.accountList = accountList;
    }

    public static AccountDB getInstance() {
        return instance;
    }

    private AccountDB() {}

    public AccountList addToAccountList(String accessKey, String secretKey, String description,
                                 String provider) {
        AccountDetails accountDetails = new AccountDetails(accessKey, secretKey, description,
                provider);
        if(accountList == null) {
            accountList = new AccountList();
        }
        accountList.add(accountDetails);
        return accountList;
    }

    public AccountList removeFromAccountList(String accessKey, String secretKey, String description,
                                             String provider) {
        AccountDetails accountDetails = new AccountDetails(accessKey, secretKey, description,
                provider);
        accountList.remove(accountDetails);
        return accountList;
    }


}
