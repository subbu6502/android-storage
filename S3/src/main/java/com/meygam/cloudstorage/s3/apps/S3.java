package com.meygam.cloudstorage.s3.apps;

import android.content.Context;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Elizabeth on 5/6/2014.
 */
public class S3 {
    private static ObjectListing objListing = null;
    public static final String ACCOUNT_NAME = "account_name";
    public static final String BUCKET_NAME = "bucket_name";
    public static final String OBJECT_NAME = "object_name";
    public static final String PREFIX = "prefix";
    public static final String FOLDER_NAME = "folder_name";

    public static AmazonS3Client getInstance(String description) {
        return MainActivity.clientManager.s3(description);
    }

    public static String validateCredentials(String accountName) {
        String toRet = "";
        try {
            getInstance(accountName).listBuckets();
            toRet = "success";
        } catch (AmazonClientException exception) {
            toRet = exception.getMessage();
        }
        return  toRet;
    }

    public static String getNumberOfBuckets(String description) {
        List<Bucket> buckets = getInstance(description).listBuckets();
        return Integer.toString(buckets.size()) + " Buckets";
    }

    public static List<String> getBucketNames(String description) {
        List<Bucket> buckets = getInstance(description).listBuckets();

        List<String> bucketNames = new ArrayList<String>( buckets.size() );
        Iterator<Bucket> bIter = buckets.iterator();
        while(bIter.hasNext()){
            bucketNames.add((bIter.next().getName()));
        }
        return bucketNames;
    }

    public static List<String> getObjectNamesForBucket( String account, String bucketName,
                                                        String prefix ) {
        ListObjectsRequest req= new ListObjectsRequest()
                .withBucketName(bucketName);
        if(!prefix.equals("/")) {
            req.setPrefix(prefix);
        }
        req.setDelimiter("/");
        ObjectListing objects = getInstance(account).listObjects( req );
        objListing = objects;
        List<String> objectNames = new ArrayList<String>( objects.getObjectSummaries().size() );
        for(int i = 0; i < objects.getObjectSummaries().size(); i++) {
            S3ObjectSummary summary = objects.getObjectSummaries().get(i);
            if(summary != null) {
                String key = summary.getKey();
                if(!prefix.equals("/")) {
                    if(!key.equals(prefix)) {
                        key = key.replace(prefix, "");
                        objectNames.add(key);
                    }
                } else {
                    objectNames.add(key);
                }
            }
        }
        /*Iterator<S3ObjectSummary> oIter = objects.getObjectSummaries().iterator();
        while(oIter.hasNext()){
            if(!prefix.equals("/")) {
                if(!oIter.next().getKey().equals(prefix)) {
                    objectNames.add(oIter.next().getKey());
                }
            } else {
                objectNames.add(oIter.next().getKey());
            }
        }*/
        for(int i = 0; i < objects.getCommonPrefixes().size(); i++) {
            String commonPrefix = objects.getCommonPrefixes().get(i);
            if(!prefix.equals("/")) {
                commonPrefix = commonPrefix.replace(prefix, "");
                objectNames.add(commonPrefix);
            } else {
                objectNames.add(commonPrefix);
            }
        }
        /*Iterator<String> commonPrefixIterator = objects.getCommonPrefixes().iterator();
        while(commonPrefixIterator.hasNext()) {
            objectNames.add(commonPrefixIterator.next());
        }*/
        // By default list objects will only return 1000 keys
        // This code will make multiple calls to fetch all keys in a bucket
        // NOTE: This could potentially cause an out of memory error
        while (objects.isTruncated()) {
            objects = getInstance(account).listNextBatchOfObjects(objects);
            for(int i = 0; i < objects.getObjectSummaries().size(); i++) {
                S3ObjectSummary summary = objects.getObjectSummaries().get(i);
                if(summary != null) {
                    String key = summary.getKey();
                    if(!prefix.equals("/")) {
                        if(!key.equals(prefix)) {
                            key = key.replace(prefix, "");
                            objectNames.add(key);
                        }
                    } else {
                        objectNames.add(key);
                    }
                }
            }
            /*oIter = objects.getObjectSummaries().iterator();
            while(oIter.hasNext()){
                if(!prefix.equals("/")) {
                    if(!oIter.next().getKey().equals(prefix)) {
                        objectNames.add(oIter.next().getKey());
                    }
                } else {
                    objectNames.add(oIter.next().getKey());
                }
            }*/
            for(int i = 0; i < objects.getCommonPrefixes().size(); i++) {
                String commonPrefix = objects.getCommonPrefixes().get(i);
                if(!prefix.equals("/")) {
                    commonPrefix = commonPrefix.replace(prefix, "");
                    objectNames.add(commonPrefix);
                } else {
                    objectNames.add(commonPrefix);
                }
            }
            /*commonPrefixIterator = objects.getCommonPrefixes().iterator();
            while(commonPrefixIterator.hasNext()) {
                objectNames.add(commonPrefixIterator.next());
            }*/
        }
        if(objectNames.size() == 0) {
            if(prefix.equals("/")) {
                objectNames.add("The bucket '"+bucketName+"' is empty");
            } else {
                objectNames.add("The folder '"+prefix+"' is empty");
            }
        }
        return objectNames;
    }

    public static List<String> getObjectNamesForBucket( String account, String bucketName,
                                                        int numItems) {
        ListObjectsRequest req= new ListObjectsRequest()
                .withBucketName(bucketName);
        req.setMaxKeys(Integer.valueOf(numItems));
        ObjectListing objects = getInstance(account).listObjects( req );
        objListing = objects;
        List<String> objectNames = new ArrayList<String>( objects.getObjectSummaries().size());
        Iterator<S3ObjectSummary> oIter = objects.getObjectSummaries().iterator();
        while(oIter.hasNext()){
            objectNames.add(oIter.next().getKey());
        }

        return objectNames;
    }

    public static List<String> getMoreObjectNamesForBucket(String account) {
        try{
            ObjectListing objects = getInstance(account).listNextBatchOfObjects(objListing);
            objListing = objects;
            List<String> objectNames = new ArrayList<String>( objects.getObjectSummaries().size());
            Iterator<S3ObjectSummary> oIter = objects.getObjectSummaries().iterator();
            while(oIter.hasNext()){
                objectNames.add(oIter.next().getKey());
            }
            return objectNames;
        } catch (NullPointerException e){
            return new ArrayList<String>();
        }

    }
    public static void createBucket( String account, String bucketName ) {
        getInstance(account).createBucket( bucketName );
    }

    public static void deleteBucket( String account, String bucketName ) {
        getInstance(account).deleteBucket(  bucketName );
    }

    public static void createObjectForBucket( String account, String bucketName,
                                              String objectName, String data ) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream( data.getBytes() );
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength( data.getBytes().length );
            getInstance(account).putObject(bucketName, objectName, bais, metadata );
        }
        catch ( Exception exception ) {
            exception.printStackTrace();
        }
    }

    public static void deleteObject( String account, String bucketName, String objectName ) {
        getInstance(account).deleteObject( bucketName, objectName );
    }

    public static S3Object getS3Object(String account, String bucketName, String folderName ) {
        S3Object s3Object = getInstance(account).getObject(bucketName, folderName);
        return s3Object;
    }

    protected static File read(Context context, InputStream inputStream, String fileName ) {
        byte[] buffer = new byte[1024];
        File file = new File(context.getExternalFilesDir(null), fileName);
        try {
            OutputStream outputStream
                    = new FileOutputStream(file);
            int count = 0;
            while ((count = inputStream.read(buffer)) != -1) {
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
            inputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return file;
        //File file = new File(fileName);
        //file.deleteOnExit();
        /*File file = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream( 8196 );
            byte[] buffer = new byte[1024];
            int length = 0;
            while ( ( length = inputStream.read( buffer ) ) > 0 ) {
                baos.write( buffer, 0, length );
            }

            FileOutputStream fos = context.openFileOutput(fileName, Activity.MODE_WORLD_READABLE);
            //new FileOutputStream(file);
            fos.write(baos.toByteArray());
            fos.close();

            file = context.getFileStreamPath(fileName);
            //return baos.toString();

        } catch(IOException exception) {

        }
        return file;*/
    }
}
