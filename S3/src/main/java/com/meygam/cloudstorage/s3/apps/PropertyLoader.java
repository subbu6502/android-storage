package com.meygam.cloudstorage.s3.apps;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Elizabeth on 5/6/2014.
 */
public class PropertyLoader {
    private boolean hasCredentials = false;
    private String accessKey = null;
    private String secretKey = null;

    private static PropertyLoader instance = null;

    public static PropertyLoader getInstance(Context context) {
        if ( instance == null ) {
            instance = new PropertyLoader(context);
        }

        return instance;
    }

    public PropertyLoader(Context context) {
        try {
            final SharedPreferences sharedPreferences = context
                    .getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
            this.accessKey = sharedPreferences.getString("accessKey", "");
            this.secretKey = sharedPreferences.getString("secretKey", "");

            /*Properties properties = new Properties();
            properties.load( context.getAssets().open( "AwsCredentials.properties" ) );

            this.accessKey = properties.getProperty( "ACCESS_KEY_ID" );
            this.secretKey = properties.getProperty( "SECRET_KEY" );*/

            if ( this.accessKey == null || this.accessKey.equals( "" ) || this.accessKey.equals( "CHANGEME" ) ||
                    this.secretKey == null || this.secretKey.equals( "" ) || this.secretKey.equals( "CHANGEME" ) ) {
                this.hasCredentials = false;
            }
            else {
                this.hasCredentials = true;
            }
        }
        catch ( Exception exception ) {
            exception.printStackTrace();
        }
    }

    public boolean hasCredentials() {
        return this.hasCredentials;
    }

    public String getAccessKey() {
        return this.accessKey;
    }

    public String getSecretKey() {
        return this.secretKey;
    }

    public void setCredentials(String accessKey, String secretKey) {
        if ( accessKey == null || accessKey.equals( "" ) ||
                secretKey == null || secretKey.equals( "" ) ) {
            this.hasCredentials = false;
        }
        else {
            this.accessKey = accessKey;
            this.secretKey = secretKey;
            this.hasCredentials = true;
        }
    }
}
